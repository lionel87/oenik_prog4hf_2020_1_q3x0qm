var searchData=
[
  ['insert_230',['Insert',['../interface_project_lamda_1_1_logic_1_1_interfaces_1_1_i_generic_service.html#a0a153670535f2a474c17b60def907da3',1,'ProjectLamda.Logic.Interfaces.IGenericService.Insert()'],['../class_project_lamda_1_1_logic_1_1_generic_service.html#a50bbc1619cdd393705233c1b05a2f2cc',1,'ProjectLamda.Logic.GenericService.Insert()'],['../interface_project_lamda_1_1_repository_1_1_interfaces_1_1_i_generic_repository.html#a47240a0b55aeb2f6e3d378071bbe1591',1,'ProjectLamda.Repository.Interfaces.IGenericRepository.Insert()'],['../class_project_lamda_1_1_repository_1_1_generic_repository.html#ac6ba35ca84efc0d96d2e76f42c960bdd',1,'ProjectLamda.Repository.GenericRepository.Insert()']]],
  ['insertanew_231',['InsertANew',['../class_project_lamda_1_1_logic_1_1_tests_1_1_lamda_tests.html#a380b12170bd811416411f9e1f46f0b6a',1,'ProjectLamda::Logic::Tests::LamdaTests']]]
];
