package oenik.LamdaWebService.Model;

import lombok.Data;

@Data
public class Lamda {

    private String name;
    private String body;
    private String lang;
}
