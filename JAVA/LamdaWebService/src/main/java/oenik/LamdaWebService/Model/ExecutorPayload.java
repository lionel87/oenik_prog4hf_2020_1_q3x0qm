package oenik.LamdaWebService.Model;

import java.util.List;
import lombok.Data;

@Data
public class ExecutorPayload {

    private final int howHappyIAmNowOnAScaleOf10 = 0;
    private List<Lamda> lamdas;
    private Object value;
}
