package oenik.LamdaWebService.Utils;

public class HTML {

    public static String Document(String head, String body) {
        return String.join(
                System.getProperty("line.separator"),
                "<!DOCTYPE html>",
                "<html>",
                "<head>",
                head,
                "</head>",
                "<body>",
                body,
                "</body>",
                "</html>",
                ""
        );
    }

    public static String Title(String title) {
        return "<title>" + title + "</title>";
    }
    
    public static String ScriptSrc(String src) {
        return "<script src=\"" + src + "\"></script>";
    }
    
    public static String Script(String script) {
        return "<script>"+script+"</script>";
    }
}
