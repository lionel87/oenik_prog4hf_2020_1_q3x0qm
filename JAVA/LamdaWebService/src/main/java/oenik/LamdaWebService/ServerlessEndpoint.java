package oenik.LamdaWebService;

import java.util.ArrayList;
import oenik.LamdaWebService.Executor.GenericExecutor;
import oenik.LamdaWebService.Model.ExecutorPayload;
import oenik.LamdaWebService.Model.ExecutorResponse;
import oenik.LamdaWebService.Model.Lamda;
import oenik.LamdaWebService.Model.LamdaResult;
import oenik.LamdaWebService.Utils.Templates;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/lamda")
public class ServerlessEndpoint {

    @GetMapping("/debug")
    ResponseEntity<String> testDemo() {

        String demoCode = "{"
                + "\"value\": [1, 2, 3],"
                + "\"lamdas\": ["
                + "{\"name\": \"kecske\","
                + " \"body\": \"return 'hello ' + (value.join(','));\","
                + " \"lang\": \"jjs\"}"
                + "]}";

        String debugger = Templates.RestDebuggerForm(demoCode);

        return ResponseEntity.ok(debugger);
    }

    @PostMapping("/exec")
    ResponseEntity<ExecutorResponse> execLamdaExpression(@RequestBody ExecutorPayload payload) {
        
        ExecutorResponse response = new ExecutorResponse();

        ArrayList<LamdaResult> lamdas = new ArrayList<>();
        response.setLamdas(lamdas);
        
        Object nextInput = payload.getValue();

        for (Lamda x : payload.getLamdas()) {
            LamdaResult lr = GenericExecutor.exec(x, nextInput);
            nextInput = lr.getResult();
            lamdas.add(lr);
        }
        
        response.setResult(nextInput);
        return ResponseEntity.ok(response);
    }
}
