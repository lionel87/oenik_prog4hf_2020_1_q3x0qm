﻿using GalaSoft.MvvmLight;
using System;

namespace WpfApp
{
    public class TaskModel : ObservableObject
    {
        private string name;
        private string langId;
        private string body;
        private DateTime createdAt;
        private DateTime updatedAt;

        public DateTime UpdatedAt
        {
            get { return updatedAt; }
            set { Set(ref updatedAt, value); }
        }

        public DateTime CreatedAt
    {
            get { return createdAt; }
            set { Set(ref createdAt, value); }
        }

        public string Body
        {
            get { return body; }
            set { Set(ref body, value); }
        }

        public string LangId
        {
            get { return langId; }
            set { Set(ref langId, value); }
        }

        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }


        public TaskModel()
        {
        }

        public void SetFrom(TaskModel model)
        {
            Name = model.Name;
            Body = model.Body;
            LangId = model.LangId;
            CreatedAt = model.CreatedAt;
            UpdatedAt = model.UpdatedAt;
        }
    }
}