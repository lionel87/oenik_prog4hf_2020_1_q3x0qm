﻿// <copyright file="IPipelineRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository.Interfaces
{
    using System.Linq;
    using ProjectLamda.Data;

    /// <summary>
    /// Repository class for pipelines table.
    /// </summary>
    public interface IPipelineRepository : IGenericRepository<pipeline>
    {
        /// <summary>
        /// Gets pipe owned tasks.
        /// </summary>
        /// <returns>The pipes and tasks.</returns>
        public IQueryable<PipeOwnedLamdas> GetPipeOwnedLamdas { get; }

        /// <summary>
        /// Gets a specific item by its id.
        /// </summary>
        /// <param name="name">Item id.</param>
        /// <returns>The desired item.</returns>
        public IQueryable<pipeline> GetById(string name);
    }
}
