﻿// <copyright file="ILangRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository.Interfaces
{
    using System.Linq;
    using ProjectLamda.Data;

    /// <summary>
    /// Repository class for langs table.
    /// </summary>
    public interface ILangRepository : IGenericRepository<lang>
    {
        /// <summary>
        /// Gets a specific item by its id.
        /// </summary>
        /// <param name="id">Item id.</param>
        /// <returns>The desired item.</returns>
        public IQueryable<lang> GetById(string id);
    }
}
