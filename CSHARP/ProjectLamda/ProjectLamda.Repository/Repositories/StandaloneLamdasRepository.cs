﻿// <copyright file="StandaloneLamdasRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository
{
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository.Interfaces;

    /// <inheritdoc/>
    public class StandaloneLamdasRepository : GenericRepository<standalone_lamdas>, IStandaloneLamdasRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StandaloneLamdasRepository"/> class.
        /// </summary>
        public StandaloneLamdasRepository()
            : base(new dbEntities())
        {
        }
    }
}
