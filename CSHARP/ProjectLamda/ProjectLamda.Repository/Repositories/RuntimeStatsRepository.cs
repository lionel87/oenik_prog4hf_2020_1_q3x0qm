﻿// <copyright file="RuntimeStatsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository
{
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository.Interfaces;

    /// <inheritdoc/>
    public class RuntimeStatsRepository : GenericRepository<runtime_stats>, IRuntimeStatsRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RuntimeStatsRepository"/> class.
        /// </summary>
        public RuntimeStatsRepository()
            : base(new dbEntities())
        {
        }

        /// <inheritdoc/>
        public IQueryable<runtime_stats> GetById(int id)
        {
            return this.SelectAll().Where(x => x.id == id);
        }
    }
}
