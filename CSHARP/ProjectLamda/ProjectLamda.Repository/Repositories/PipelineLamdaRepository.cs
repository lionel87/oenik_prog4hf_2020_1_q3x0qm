﻿// <copyright file="PipelineLamdaRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository
{
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository.Interfaces;

    /// <inheritdoc/>
    public class PipelineLamdaRepository : GenericRepository<lamdas_pipelines>, IPipelineLamdaRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineLamdaRepository"/> class.
        /// </summary>
        public PipelineLamdaRepository()
            : base(new dbEntities())
        {
        }

        /// <inheritdoc/>
        public IQueryable<lamdas_pipelines> GetById(string pipe, string lamda)
        {
            return this.SelectAll().Where(x => x.pipeline_name == pipe && x.lamda_name == lamda);
        }
    }
}
