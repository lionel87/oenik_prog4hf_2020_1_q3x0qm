﻿// <copyright file="LamdaRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository
{
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository.Interfaces;

    /// <inheritdoc/>
    public class LamdaRepository : GenericRepository<lamda>, ILamdaRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LamdaRepository"/> class.
        /// </summary>
        public LamdaRepository()
            : base(new dbEntities())
        {
        }

        /// <summary>
        /// Gets all lamdas which is part of a pipeline.
        /// </summary>
        public IQueryable<lamda> AllPipedLamdas
        {
            get
            {
                using var ctx = new dbEntities();

                return from l in this.SelectAll()
                       join lp in ctx.lamdas_pipelines on l.name equals lp.lamda_name
                       select l;
            }
        }

        /// <inheritdoc/>
        public IQueryable<lamda> GetById(string name)
        {
            return this.SelectAll().Where(x => x.name == name);
        }

        /// <inheritdoc/>
        public IQueryable<lamda> GetByPipe(string name)
        {
            using var ctx = new dbEntities();

            return (from pip in ctx.pipelines
                   join pip_lam in ctx.lamdas_pipelines on pip.name equals pip_lam.pipeline_name
                   join lam in ctx.lamdas on pip_lam.lamda_name equals lam.name
                   where pip.name == name
                   orderby pip_lam.position ascending
                   select lam).ToList().AsQueryable();
        }
    }
}
