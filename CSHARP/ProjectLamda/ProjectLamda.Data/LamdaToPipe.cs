﻿// <copyright file="LamdaToPipe.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Data
{
    using System.Collections.Generic;
    using ProjectLamda.Data;

    /// <summary>
    /// Container for pipeownedlamdas result.
    /// </summary>
    public class LamdaToPipe
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LamdaToPipe"/> class.
        /// </summary>
        /// <param name="pipeline">The pipeline.</param>
        /// <param name="lamdas">Tle lamda.</param>
        public LamdaToPipe(string pipeline, string lamdas)
        {
            this.Lamdas = lamdas;
            this.Pipeline = pipeline;
        }

        /// <summary>
        /// Gets the lamda for this pipe.
        /// </summary>
        public string Lamdas { get; }

        /// <summary>
        /// Gets the pipeline name for this.
        /// </summary>
        public string Pipeline { get; }
    }
}