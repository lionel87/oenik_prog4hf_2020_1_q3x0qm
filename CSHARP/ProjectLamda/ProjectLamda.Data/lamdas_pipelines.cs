//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectLamda.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class lamdas_pipelines
    {
        public string lamda_name { get; set; }
        public string pipeline_name { get; set; }
        public int position { get; set; }
        public System.DateTime created_at { get; set; }
        public System.DateTime updated_at { get; set; }
    
        public virtual lamda lamda { get; set; }
        public virtual pipeline pipeline { get; set; }
    }
}
