﻿using System;

namespace WEBAPIConsoleClient
{
    public class TaskModel
    {
        public string Name { get; set; }
        public string Body { get; set; }
        public string LangId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public TaskModel()
        {
        }

        public override string ToString()
        {
            return $"{Name} ({LangId}, {CreatedAt}, {UpdatedAt}) = {Body}";
        }
    }
}