﻿// <copyright file="LamdaTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using ProjectLamda.Data;
    using ProjectLamda.Repository.Interfaces;

    /// <summary>
    /// LamdaTests.
    /// </summary>
    [TestFixture]
    public class LamdaTests
    {
        /// <summary>
        /// Testing select all CRUD.
        /// </summary>
        [Test]
        public void SelectAllDoesNotFail()
        {
            // Arrange
            Mock<ILamdaRepository> repo = new Mock<ILamdaRepository>();
            LamdaService svc = new LamdaService(repo.Object);

            // Act
            var result = svc.SelectAll();

            // Assert
            Assert.That(result, Is.Not.Null);
        }

        /// <summary>
        /// Testing insert into.
        /// </summary>
        [Test]
        public void InsertANew()
        {
            IList<lamda> lamdas = new List<lamda>()
            {
                new lamda()
                {
                    name = "hello",
                    lang_id = "jjs",
                    body = "return 'hello ' + value;",
                },
            };

            Mock<ILamdaRepository> repo = new Mock<ILamdaRepository>();
            repo.Setup(mr => mr.SelectAll()).Returns(lamdas.AsQueryable());
            repo.Setup(mr => mr.Insert(It.IsAny<lamda>())).Callback((lamda item) => lamdas.Add(item));
            LamdaService svc = new LamdaService(repo.Object);

            Assert.AreEqual(1, svc.SelectAll().Count);

            svc.Insert(new lamda()
            {
                name = "hello",
                lang_id = "jjs",
                body = "return 'hello ' + value;",
            });

            Assert.AreEqual(2, svc.SelectAll().Count);
        }

        /// <summary>
        /// Testing update.
        /// </summary>
        [Test]
        public void UpdateTest()
        {
            IList<lamda> lamdas = new List<lamda>()
            {
                new lamda()
                {
                    name = "hello",
                    lang_id = "jjs",
                    body = "return 'hello ' + value;",
                },
            };

            Mock<ILamdaRepository> repo = new Mock<ILamdaRepository>();
            repo.Setup(mr => mr.SelectAll()).Returns(lamdas.AsQueryable());
            repo.Setup(mr => mr.Update(It.IsAny<lamda>())).Callback((lamda item) => lamdas[0] = item);
            LamdaService svc = new LamdaService(repo.Object);

            lamda ll = svc.SelectAll().First();
            ll.name = "kecske";
            svc.Update(ll);

            Assert.AreEqual("kecske", svc.SelectAll().First().name);
        }

        /// <summary>
        /// Testing delete.
        /// </summary>
        [Test]
        public void DeleteTest()
        {
            IList<lamda> lamdas = new List<lamda>()
            {
                new lamda()
                {
                    name = "hello",
                    lang_id = "jjs",
                    body = "return 'hello ' + value;",
                },
            };

            Mock<ILamdaRepository> repo = new Mock<ILamdaRepository>();
            repo.Setup(mr => mr.SelectAll()).Returns(lamdas.AsQueryable());
            repo.Setup(mr => mr.Delete(It.IsAny<lamda>())).Callback((lamda item) => lamdas.Remove(item));
            LamdaService svc = new LamdaService(repo.Object);

            lamda ll = svc.SelectAll().First();
            svc.Delete(ll);

            Assert.AreEqual(0, svc.SelectAll().Count);
        }

        /// <summary>
        /// Testing get by id.
        /// </summary>
        [Test]
        public void GetByIdTest()
        {
            IList<lamda> lamdas = new List<lamda>()
            {
                new lamda()
                {
                    name = "hello",
                    lang_id = "jjs",
                    body = "return 'hello ' + value;",
                },
            };

            Mock<ILamdaRepository> repo = new Mock<ILamdaRepository>();
            repo.Setup(mr => mr.GetById(It.IsAny<string>())).Returns((string item) => lamdas.Where(x => x.name == item).AsQueryable());
            repo.Setup(mr => mr.Delete(It.IsAny<lamda>())).Callback((lamda item) => lamdas.Remove(item));
            LamdaService svc = new LamdaService(repo.Object);

            lamda ll = svc.GetById("hello");

            Assert.AreEqual("jjs", ll.lang_id);
        }
    }
}
