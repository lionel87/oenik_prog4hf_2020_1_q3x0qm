﻿using ProjectLamda.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectLamda.WebApp.Models
{
    public class CreateEditTaskModel
    {
        public lamda Task { get; set; }
        public List<lang> Langs { get; set; }
    }
}