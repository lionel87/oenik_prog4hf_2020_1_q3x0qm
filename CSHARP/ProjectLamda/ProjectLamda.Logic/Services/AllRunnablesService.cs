﻿// <copyright file="AllRunnablesService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository;

    /// <summary>
    /// Service implementation of the corresponding repository.
    /// </summary>
    public class AllRunnablesService : GenericService<AllRunnablesRepository, all_runnables>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AllRunnablesService"/> class.
        /// </summary>
        /// <param name="repository">Repository instance.</param>
        public AllRunnablesService(AllRunnablesRepository repository)
            : base(repository)
        {
        }

        /// <summary>
        /// Gets a specific item by id.
        /// </summary>
        /// <param name="id">Item id.</param>
        /// <returns>Item referenced by given id.</returns>
        public all_runnables GetById(string id)
        {
            return this.Repository.GetById(id).First();
        }
    }
}
