﻿// <copyright file="LamdaRunnerService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using Newtonsoft.Json.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository;

    /// <summary>
    /// Service.
    /// </summary>
    public static class LamdaRunnerService
    {
        /// <summary>
        /// Runs a lamda or a pipeline with a string input value.
        /// </summary>
        /// <param name="name">Lamda or a pipeline name.</param>
        /// <param name="value">value to calc with.</param>
        /// <returns>Run results.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "Úgy nem megy.")]
        public static LamdaRunResult RunThis(string name, string value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            List<lamda> lls;

            LamdaService ls = new LamdaService(new LamdaRepository());
            lamda ll = ls.GetById(name);

            if (ll != null)
            {
                lls = new List<lamda>()
                {
                    ll,
                };
            }
            else
            {
                lls = new LamdaRepository().GetByPipe(name).ToList();
            }

            var serialized = SerializeToJson(value, lls);

            LamdaRunResult result;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:8080");
                client.DefaultRequestHeaders.Add("User-Agent", "ProjectLamda Client");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                using var context = new StringContent(serialized, Encoding.UTF8, "application/json");
                var response = client.PostAsync("lamda/exec", context).Result;
                response.EnsureSuccessStatusCode();

                var s = response.Content.ReadAsStringAsync().Result;
                var x = JObject.Parse(s);

                // result = response.Content.ReadAsAsync<LamdaRunResult>().Result;
                result = new LamdaRunResult(
                    x["result"].ToString(),
                    x["lamdas"].Select(x => new LamdaRuntimeStat()
                    {
                        Name = (string)x["name"],
                        Result = x["result"].ToString(),
                        Runtime = (int)x["runtime"],
                        StartedAt = (string)x["startedAt"],
                    }).ToList());
            }

            return result;
        }

        private static string SerializeToJson(int value, List<lamda> ls)
        {
            return "{\"value\": " + value + ", \"lamdas\": " + LamdaToJson(ls) + "}";
        }

        private static string SerializeToJson(string value, List<lamda> ls)
        {
            return "{\"value\": \"" + value.Replace("\"", "\\\"") + "\", \"lamdas\": " + LamdaToJson(ls) + "}";
        }

        private static string LamdaToJson(List<lamda> ls)
        {
            string res = string.Join(",", ls.Select(x => LamdaToJson(x)).ToList());

            return "[" + res + "]";
        }

        private static string LamdaToJson(lamda ls)
        {
            return "{" +
                "\"name\": \"" + ls.name + "\"," +
                "\"lang\": \"" + ls.lang_id + "\"," +
                "\"body\": \"" + ls.body.Replace("\\", "\\\\").Replace("\"", "\\\"") + "\"" +
                "}";
        }
    }
}
