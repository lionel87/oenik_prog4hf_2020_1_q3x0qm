﻿// <copyright file="PipelineService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository.Interfaces;

    /// <summary>
    /// Service implementation of the corresponding repository.
    /// </summary>
    public class PipelineService : GenericService<IPipelineRepository, pipeline>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineService"/> class.
        /// </summary>
        /// <param name="repository">Repository instance.</param>
        public PipelineService(IPipelineRepository repository)
            : base(repository)
        {
        }

        /// <summary>
        /// Gets the pipe owned lamdas lsit.
        /// </summary>
        public List<PipeOwnedLamdas> GetPipeOwnedLamdas
        {
            get
            {
                return this.Repository.GetPipeOwnedLamdas.ToList();
            }
        }

        /// <summary>
        /// Gets a specific item by id.
        /// </summary>
        /// <param name="name">Item id.</param>
        /// <returns>Item referenced by given id.</returns>
        public pipeline GetById(string name)
        {
            return this.Repository.GetById(name).First();
        }
    }
}
