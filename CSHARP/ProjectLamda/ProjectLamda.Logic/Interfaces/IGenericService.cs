﻿// <copyright file="IGenericService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Generic CRUD service interface.
    /// </summary>
    /// <typeparam name="TEntity">Entity type.</typeparam>
    public interface IGenericService<TEntity>
    {
        /// <summary>
        /// Gets all the items.
        /// </summary>
        /// <returns>All the items.</returns>
        public List<TEntity> SelectAll();

        /// <summary>
        /// Stores the item.
        /// </summary>
        /// <param name="item">Item to insert.</param>
        public void Insert(TEntity item);

        /// <summary>
        /// Updates an existing item.
        /// </summary>
        /// <param name="item">Item to update.</param>
        public void Update(TEntity item);

        /// <summary>
        /// Deletes this item.
        /// </summary>
        /// <param name="item">Item to delete.</param>
        public void Delete(TEntity item);
    }
}
